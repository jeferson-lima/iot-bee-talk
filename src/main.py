
import argparse
import logging
import sounddevice as sd
from scipy.io.wavfile import write
import time

'''
Honey bees produce many frequencies of vibration and sound
    from less than 10 to more than 1000 Hz.
'''

parser = argparse.ArgumentParser(description='IOT BEE-TALK.')
parser.add_argument('--version', action='version', version='0.0.1')
# parser.add_argument('--fs', type=int, default=44100, help='Frequency Sampling')
parser.add_argument('--fs', type=int, default=1000, help='Frequency Sampling')
parser.add_argument('--record', type=int, default=2, help='Record Time')
args = parser.parse_args()

# Parameters
fs = args.fs             # Sample rate
seconds = args.record    # Duration of recording
output_folder = '/tmp/records/'

# config logger
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s-%(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename= output_folder+ 'bee.log',
                    filemode='a')
logger = logging.getLogger(__name__)

logger.info(f"Start Record! Parameters: time record {seconds}s, frequency {fs}Hz")
myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2)
sd.wait()  # Wait until recording is finished
logger.info(f"Finish Record! It was saved in {output_folder}")


dt_str = time.strftime("%Y-%m-%d-%H:%M:%S")
write(f"{output_folder}rec_{str(seconds)}s_{dt_str}.wav", fs, myrecording) 