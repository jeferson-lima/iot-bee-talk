.PHONY: help install run clean

.DEFAULT: help
help:
	@echo "make install"
	@echo "       Prepare Development Environment"
	@echo "make run"
	@echo "       Add Schedule Audio Record"
	@echo "make clean"
	@echo "       Remove output files"

install:
	python3 -m pip install -r requirements.txt
	mkdir ~/records

run:
	@echo "Program was scheduled"
		# * * * * * command to be executed
		# - - - - -
		# | | | | |
		# | | | | ----- Day of week (0 - 7) (Sunday=0 or 7)
		# | | | ------- Month (1 - 12)
		# | | --------- Day of month (1 - 31)
		# | ----------- Hour (0 - 23)
		# ------------- Minute (0 - 59)
	
	# #write out current crontab
	@crontab -l > mycron
	# #echo new cron into cron file
	@echo "*/1 * * * * $(USER) python3 $(PWD)/src/main.py" >> mycron
	# #install new cron file
	@crontab mycron
	@rm mycron

clean:
	@rm -rf *.eggs *.egg-info .cache .mypy_cache/ .pytest_cache/

